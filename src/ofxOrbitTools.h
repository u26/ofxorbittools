#pragma once

#include "ofMain.h"

#include "coreLib.h"
#include "orbitLib.h"


class ofxOrbitTools {

    public:
        ofxOrbitTools();
        ~ofxOrbitTools();
    
        void setup( string sat_name, string tle1, string tle2 );
        void update( time_t time );
    
        void exit();
        double getLat();
        double getLon();
        double getAlt();
    
        time_t getTime();
        ofVec3f getPosition();
        ofVec3f getVelocity();
        void convertTimeToStr(const time_t &time, char *str);
    
	private:

        cTle* tleSGP4;
        cSatellite* satSGP4;

        cEci* eci;
        cEciTime* ecitime;
        
        ofVec3f mPos3f;
        ofVec3f mVel3f;
        
        time_t mDate;
        double mLat;
        double mLon;
        double mAlt;
    
};
