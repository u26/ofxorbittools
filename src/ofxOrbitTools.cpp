#include "ofxOrbitTools.h"

ofxOrbitTools::ofxOrbitTools(){
    
}

ofxOrbitTools::~ofxOrbitTools(){
}

void ofxOrbitTools::setup(string sat_name, string tle1, string tle2){

    tleSGP4 = new cTle( sat_name, tle1, tle2 );
    
    satSGP4 = new cSatellite(*tleSGP4);

}

void ofxOrbitTools::update( time_t time ){
    
    cJulian current_julian(time);
    cEciTime eci = satSGP4->PositionEci( current_julian );
    cGeo geo(eci, current_julian);

    mLat = geo.LatitudeDeg();
    mLon = geo.LongitudeDeg();
    mAlt = geo.AltitudeKm();

    mDate = eci.Date().ToTime();

    // ECI(Earth-centered inertial)
    mPos3f = ofVec3f( eci.Position().m_y,
                     eci.Position().m_z,
                     eci.Position().m_x );

    mVel3f = ofVec3f( eci.Velocity().m_y,
                     eci.Velocity().m_z,
                     eci.Velocity().m_x );
}


time_t ofxOrbitTools::getTime(){
    
    return mDate;
}

double ofxOrbitTools::getLat(){

    return mLat;
}

double ofxOrbitTools::getLon(){

    return mLon;
}

double ofxOrbitTools::getAlt(){
    
    return mAlt;
}

ofVec3f ofxOrbitTools::getPosition(){
    
    return mPos3f;
}

ofVec3f ofxOrbitTools::getVelocity(){
    
    return mVel3f;
}


void ofxOrbitTools::exit(){
    
    free(tleSGP4);
    free(satSGP4);
}


void ofxOrbitTools::convertTimeToStr(const time_t &time, char *str)
{
    struct tm *date;
    date = gmtime(&time);
	strftime(str, 255, "%Y-%m-%d %H:%M:%S", date);
    
    
//	struct tm *date;
//	date = localtime(&time);
//	strftime(str, 255, "%Y-%m-%d %H:%M:%S", date);
}
