#pragma once

#include "ofMain.h"
#include "ofxOrbitTools.h"

#include "ofGLProgrammableRenderer.h"


class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
        void setViewPosition( int mode );
    
        ofxOrbitTools sat[2];
        
        ofEasyCam cam;
        ofBoxPrimitive box[2];
        
        ofImage texture;
        ofSpherePrimitive earth;

        long l_dt = 0;

        int EARTH_R_HALF = 6378;

        
        vector<ofVec3f> orb[2];

        // display flag
        int  i_selectImg = 0;
        int  i_selectView = 0;
        bool b_displayWireframe;
        bool b_mouse;

};
