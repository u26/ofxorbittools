#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    
    
    ofEnableSmoothing();
    ofEnableAlphaBlending();
    ofEnableNormalizedTexCoords();

    ofSetFullscreen(true);
    ofBackground(10,10,10);
    
    string str1 = "HODOYOSHI-3";
    string str2 = "1 40015U 14033F   14233.78826749  .00000369  00000-0  59433-4 0  1937";
    string str3 = "2 40015  97.9755 127.7151 0038850   7.8302 352.3491 14.76871410  9300";
    
    sat[0].setup(str1,str2,str3);

    str1 = "HODOYOSHI-4";
    str2 = "1 40011U 14033B   14233.47987029  .00000391  00000-0  59371-4 0  1934";
    str3 = "2 40011  97.9772 127.6648 0028797  17.4646 342.7555 14.79349270  9268";

    sat[1].setup(str1,str2,str3);
    
    box[0].set(80);
    box[1].set(80);
    
    cam.setFov(60.f);
    cam.setFarClip(1000000);
    cam.setDistance(20000);
//    cam.setVFlip(true);
    
    //use image greenwich(longitude:0) is left side.
    texture.loadImage("earth.png");
    i_selectImg = 1;
    
    //calc orbit for 1 hour.
    for( int i=0; i < 1*60*60; i++ ){

        time_t now = time(NULL);

        sat[0].update(now + i);
        sat[1].update(now + i);

        orb[0].push_back(sat[0].getPosition());
        orb[1].push_back(sat[1].getPosition());
    }
}

//--------------------------------------------------------------
void ofApp::update(){

    l_dt++;
    sat[0].update(time(NULL) + l_dt);
    sat[1].update(time(NULL) + l_dt);
    
}

//--------------------------------------------------------------
void ofApp::draw(){
    
    ofVec3f pos[2];
    pos[0] = sat[0].getPosition();
    pos[1] = sat[1].getPosition();

    setViewPosition( i_selectView );
    
    cam.begin();
    ofPushMatrix();

    //rotate earth
    double r = (360.0 / 86400);  //86400 = 24*60*60
    
//    ofRotateY( time(NULL)* r); // ??? OFFSET: how to calc offset...
    ofRotateY( l_dt* r);

    ofEnableDepthTest();

    texture.bind();
    ofFill();
    ofSetColor(255, 255, 255);
    earth.setPosition(0,0,0);
    earth.setRadius(EARTH_R_HALF);
    earth.draw();
    texture.unbind();

    if( b_displayWireframe ){
        
        ofSetColor(255, 255, 255,50);
        earth.drawWireframe();
    }

    ofPopMatrix();

    //-------------------
    // draw orbit
    //-------------------
    ofPolyline line;
    for( int i=0; i<orb[0].size()-1; i++ ){
        line.addVertex( orb[0].at(i) );
    }
    ofSetColor(255, 255, 255, 80);
    line.draw();

    line.clear();
    for( int i=0; i<orb[1].size()-1; i++ ){
        line.addVertex( orb[1].at(i) );
    }
    ofSetColor(255, 255, 255, 80);
    line.draw();

    //-------------------
    // draw sat box
    //-------------------
    box[0].setPosition( pos[0].x, pos[0].y, pos[0].z);
    box[1].setPosition( pos[1].x, pos[1].y, pos[1].z);
    ofSetColor( 255, 0, 0);
    box[0].drawWireframe();
    
    ofSetColor(255, 255, 0);
    box[1].drawWireframe();
    
    cam.end();
    
    
    //------------------------------
    // DISPLAY LOG
    //------------------------------
    int lh = 15;

    char c_dt[256];
    sat[0].convertTimeToStr(sat[0].getTime(),c_dt);
    string s_dt = c_dt;
    ofDrawBitmapString( "UTC:"+ s_dt, 10, lh );

    sat[0].convertTimeToStr(sat[0].getTime()+(9*60*60),c_dt);
    s_dt = c_dt;
    ofDrawBitmapString( "JST:"+ s_dt, 10, lh * 2 );

    ofSetColor( 255, 0, 0);
    ofRect(10, lh * 4-10, 10, 10);
    ofSetColor(255, 255, 255);
    ofDrawBitmapString( "HODOYOSHI-3", 30, lh * 4);
    ofDrawBitmapString( "LAT = " + ofToString(sat[0].getLat(), 6), 10, lh * 5);
    ofDrawBitmapString( "LNG = " + ofToString(sat[0].getLon(), 6), 10, lh * 6);

    ofSetColor(255, 255, 0);
    ofRect(10, lh * 8-10, 10, 10);
    ofSetColor(255, 255, 255);
    ofDrawBitmapString( "HODOYOSHI-4", 30, lh * 8);
    ofDrawBitmapString( "LAT = " + ofToString(sat[1].getLat(), 6), 10, lh * 9);
    ofDrawBitmapString( "LNG = " + ofToString(sat[1].getLon(), 6), 10, lh * 10);
    
    
    //------------------------------
    // DISPLAY LOG
    //------------------------------
    ofDrawBitmapString( "\'f\' = fullscreen", 30, lh * 12);
    ofDrawBitmapString( "\'w\' = show/hide wire frame", 30, lh * 13);
    ofDrawBitmapString( "\'1\' = change texture", 30, lh * 14);
    
}


//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

    if(key == 'f' ||key == 'F'){

        ofToggleFullscreen();
    }
    else if(key == 'w' ||key == 'W'){
        
        b_displayWireframe = !b_displayWireframe;
    }
    
    if(key=='1'){
        
        if( i_selectImg == 0 ){
            ofBackground(10,10,10);
            texture.loadImage("earth.png");
            i_selectImg++;

        }else if( i_selectImg == 1 ){
            ofBackground(255,255,255);
            texture.loadImage("earth_hight.png");
            i_selectImg++;

        }else if( i_selectImg == 2 ){
            ofBackground(255,255,255);
            texture.loadImage("earth_night.png");
            i_selectImg = 0;
        }
        
    }else if( key == 'v' ){
        
        i_selectView++;
        if( i_selectView > 2 ){
            i_selectView = 0;
        }
    }else if ( key == 'm') {
        
        b_mouse = !b_mouse;
        
        if(b_mouse){
            ofShowCursor();
        }else{
            ofHideCursor();
        }
    }
}

void ofApp::setViewPosition( int mode ){
    
    if( mode == 0 ){
        
        cam.setPosition(0, 0, 20000);
        cam.setDistance(20000);
        cam.setTarget(earth.getPosition());
        box[0].set(100);
        box[1].set(100);

    }else if( mode == 1 ){
        
        cam.setPosition(sat[0].getPosition() * 2.f);
        cam.setTarget(earth.getPosition());
        box[0].set(50);
        box[1].set(50);
        
    }else if( mode == 2 ){
        
        cam.setPosition(sat[1].getPosition() * 2.f);
        cam.setTarget(earth.getPosition());
    }
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
